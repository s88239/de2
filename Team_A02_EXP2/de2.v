module de2(KEY, SW, CLOCK_50, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7, LCD_DATA, LCD_EN, LCD_RW, LCD_RS, LCD_ON, GPIO);
input [3:0] KEY;
input [17:0] SW;
input CLOCK_50;
output [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7;
output [7:0] LCD_DATA;
output LCD_EN, LCD_RW, LCD_RS,LCD_ON;
output [1:0] GPIO;
wire [6:0] hex6, hex7;
wire [6:0] hex24, hex25, hex26;
wire [6:0] hex50, hex51, hex52, hex53, hex54, hex56, hex57;
wire [7:0] lcd, lcd2;
wire lcden, lcdrw, lcdrs, lcden2, lcdrw2, lcdrs2;
wire [1:0] gpio, gpio2;


reg [2:0] key;
reg [2:0] func_reg;
assign func = func_reg;
initial
begin
key = 3'b000;
func_reg = 3'b000;
end

always @(posedge CLOCK_50)
begin
	
	if(!KEY[0]) func_reg = 3'b001;
	if(!KEY[1]) func_reg = 3'b010;
	if(!KEY[2]) func_reg = 3'b011;
	if(!key[2] && key[1])
	begin
		if(func_reg==3'b100)  begin		
			func_reg = 3'b101;
		end
		else begin
			func_reg = 3'b100;
		end
	end
	
	key <= {key[1:0],KEY[3]};
end



f1 fuck1(.clk(CLOCK_50),.sw(SW[17:16]),.led1(hex6),.led2(hex7));
exp2 fuck2(.sw(SW[17:6]),.led7_1(hex26),.led7_2(hex24),.led7_3(hex25));
function3 fuck3(.clk(CLOCK_50), .sw1(SW[17:15]), .sw2(SW[14:12]), .lcd(lcd), .lcden(lcden), .lcdrs(lcdrs), .lcdrw(lcdrw));
function4 fuck4(.clk(CLOCK_50), .channel(SW[0]), .sw1(SW[17:15]), .sw2(SW[14:12]), .sw3(SW[11:9]), .sw4(SW[8:6]), .lcd(lcd2), .lcden(lcden2), .lcdrs(lcdrs2), .lcdrw(lcdrw2), .o_gpio(gpio));
f5 fuck5(.f_clk(CLOCK_50),.ch_choose(SW[0]),.ch1_fq(SW[17:15]),.ch1_duty(SW[14:12]),.ch2_fq(SW[11:9]),.ch2_duty(SW[8:6]),.o_gpio(gpio2),.fq1(hex50),.fq2(hex51),.fq3(hex52),.fq4(hex53),.fq5(hex54),.duty1(hex56),.duty2(hex57));

assign  {LCD_ON , LCD_DATA , LCD_EN , LC_RW, LCD_RS,HEX0,HEX1,HEX2,HEX3,HEX4,HEX5,HEX6,HEX7,GPIO} =(func_reg==3'b001)?	{1'b0, lcd, lcden, lcdrw, lcdrs, 7'b1111111, 7'b1111111  , 7'b1111111  , 7'b1111111 , 7'b1111111  , 7'b1111111  , hex6 ,hex7 , 2'b00} 

 : (func_reg==3'b010)? {1'b0, lcd, lcden, lcdrw, lcdrs, 7'b1111111, 7'b1111111  , 7'b1111111  , 7'b1111111 , hex24  , hex25  , hex26 ,7'b1111111 , 2'b00} 
 
 : (func_reg==3'b011)? {1'b1, lcd, lcden, lcdrw, lcdrs, 7'b1111111, 7'b1111111  , 7'b1111111  , 7'b1111111  , 7'b1111111  , 7'b1111111  ,7'b1111111 ,7'b1111111 , 2'b00}

 : (func_reg==3'b100)? {1'b1, lcd2, lcden2, lcdrw2, lcdrs2,7'b1111111, 7'b1111111  , 7'b1111111  , 7'b1111111  , 7'b1111111  , 7'b1111111  ,7'b1111111 ,7'b1111111 , gpio}

 : (func_reg==3'b101)? {1'b0, lcd, lcden, lcdrw, lcdrs, hex50, hex51, hex52, hex53, hex54, 7'b1111111,hex56 ,hex57 ,gpio2}
 
 : {1'b0, lcd, lcden, lcdrw, lcdrs, 7'b1111111, 7'b1111111  , 7'b1111111  , 7'b1111111  , 7'b1111111  , 7'b1111111  ,7'b1111111 ,7'b1111111 , 2'b00};


endmodule

module f1 (clk,sw,led1,led2);
input [1:0] sw;//sw[1] controls sw17, sw[0] controls sw16
input clk;
output [6:0] led1, led2;
reg [6:0] led1_latch;
reg [6:0] led2_latch;
reg [25:0] count;
reg [3:0] digit1, digit2;//digit1 record the single-digit, digit2 record the 10-digit
assign led1 = led1_latch;//later led number
assign led2 = led2_latch;//head led number

initial
begin
	digit1 = 4'd0;
	digit2 = 4'd0;
	count = 26'd0;
end

always @ (posedge clk or posedge sw[0])//clock
begin
	if(sw[0]) begin//stop, reset counter to 00
			digit1 <= 4'd0;
			digit2 <= 4'd0;
			led1_latch <= 7'b1000000;
			led2_latch <= 7'b1000000;
			count = 26'd0;
	end
	else begin//start counting
		count <= count + 26'd1;
		if(count==26'd50000000) begin
			count <= 26'd0;
			if(sw[1]==1) begin//count upward
				if(digit1==4'd9) begin
					digit1 <= 4'd0;
					if(digit2==4'd9) digit2 <= 4'd0;
					else digit2 <= digit2 + 4'd1;
				end
				else digit1 <= digit1 + 4'd1;
			end
			else begin//sw[1]==0, count downward
				if(digit1==0) begin
					digit1 <= 4'd9;
					if(digit2==0) digit2 <= 4'd9;
					else digit2 <= digit2 - 4'd1;
				end
				else digit1 <= digit1 - 4'd1;
			end
			if(digit1==0) led1_latch <= 7'b1000000;
			else if (digit1==1) led1_latch <= 7'b1111001;
			else if (digit1==2) led1_latch <= 7'b0100100;
			else if (digit1==3) led1_latch <= 7'b0110000;
			else if (digit1==4) led1_latch <= 7'b0011001;
			else if (digit1==5) led1_latch <= 7'b0010010;
			else if (digit1==6) led1_latch <= 7'b0000010;
			else if (digit1==7) led1_latch <= 7'b1111000;
			else if (digit1==8) led1_latch <= 7'b0000000;
			else if (digit1==9) led1_latch = 7'b0011000;
			if(digit2==0) led2_latch <= 7'b1000000;
			else if (digit2==1) led2_latch <= 7'b1111001;
			else if (digit2==2) led2_latch <= 7'b0100100;
			else if (digit2==3) led2_latch <= 7'b0110000;
			else if (digit2==4) led2_latch <= 7'b0011001;
			else if (digit2==5) led2_latch <= 7'b0010010;
			else if (digit2==6) led2_latch <= 7'b0000010;
			else if (digit2==7) led2_latch <= 7'b1111000;
			else if (digit2==8) led2_latch <= 7'b0000000;
			else if (digit2==9) led2_latch <= 7'b0011000;
		end
	end
end
endmodule


module exp2 (sw,led7_1,led7_2,led7_3);

input [11:0] sw;  //
output [6:0] led7_1,led7_2,led7_3;

reg[5:0] a ;
reg[5:0] b ;
reg[6:0] sum;

reg[6:0]led1_latch;
reg[6:0]led2_latch;
reg[6:0]led3_latch;

assign led7_1 = led1_latch; //symbol
assign led7_2 = led2_latch; //units
assign led7_3 = led3_latch; //tens

//integer sum_tens;
//integer sum_units;
reg[6:0] sumo;

initial
begin
	a = 6'd0;
	b = 6'd0;
	sum = 6'd0; 
	sumo = 0;
	//sum_tens=0;
	//sum_units=0;
	led1_latch = 7'b1111111;
	led2_latch = 7'b1000000;
	led3_latch = 7'b1111111;
end

always@(sw)
begin
if(sw[10]) //if sw2 high , do add or sub
	begin
	a = {sw[9],sw[9:5]};
	b = {sw[4],sw[4:0]};
	
	if(sw[11]==1)
	sum=a+b;
	else if(sw[11]==0)
	sum=a-b;
	
	if(sum[5]==1)  // <0
		begin
		led1_latch = 7'b0111111; //print '-'
		sumo=64-(sum[5:0]);
		//sum_tens = sumo/10;  // tens
		//sum_units = sumo%10; //units
		end
	else				// >0
		begin
	   led1_latch = 7'b1111111; //print ' '
		sumo = sum[4:0];
		//sum_tens = sumo/10;  // tens
		//sum_units = sumo%10; //units
		end
		if(sumo==32)
			begin
			led2_latch = 7'b0100100;
			led3_latch = 7'b0110000;
			end
	case(sumo)
		6'h00, 6'h0A, 6'h14, 6'h1E: led2_latch = 7'b1000000; //sumo=0,10,20,30
		6'h01, 6'h0B, 6'h15, 6'h1F: led2_latch = 7'b1111001; //sumo=1,11,21,31
		6'h02, 6'h0C, 6'h16: led2_latch = 7'b0100100;		  //sumo=2,12,22			
		6'h03, 6'h0D, 6'h17: led2_latch = 7'b0110000;		  //sumo=3,13,23
		6'h04, 6'h0E, 6'h18: led2_latch = 7'b0011001;		  //sumo=4,14,24
		6'h05, 6'h0F, 6'h19: led2_latch = 7'b0010010;		  //sumo=5,15,25
		6'h06, 6'h10, 6'h1A: led2_latch = 7'b0000010;		  //sumo=6,16,26
		6'h07, 6'h11, 6'h1B: led2_latch = 7'b1111000;		  //sum0=7,17,27	
		6'h08, 6'h12, 6'h1C: led2_latch = 7'b0000000;        //sumo=8,18,28
		6'h09, 6'h13, 6'h1D: led2_latch = 7'b0011000;		  //sumo=9,19,29
   endcase
	
	case(sumo)
		6'h1E,6'h1F: led3_latch = 7'b0110000; 					//tens='3'
		6'h14,6'h15,6'h16,6'h17,6'h18,6'h19,6'h1A,6'h1B,6'h1C,6'h1D: led3_latch = 7'b0100100; //tens='2'
		6'h0A,6'h0B,6'h0C,6'h0D,6'h0E,6'h0F,6'h10,6'h11,6'h12,6'h13: led3_latch = 7'b1111001; //tens='1'
		6'h00,6'h01,6'h02,6'h03,6'h04,6'h05,6'h06,6'h07,6'h08,6'h09: led3_latch = 7'b1000000; //tens print '0' 
	endcase
	
	end
end

endmodule

module function3(clk, sw1, sw2, lcd, lcden, lcdrs, lcdrw);
	input clk;
   input [2:0] sw1;
   input [2:0] sw2;
   output [7:0] lcd;
	output lcden;
   output lcdrs;
   output lcdrw;
	
	reg [7:0] lcd_words;

	reg [31:0] t;
	
	reg [7:0] pos;
	reg [2:0] pre_sw1;
	reg [2:0] pre_sw2;
	reg lcden_reg;
	reg lcdrs_reg;
	reg lcdrw_reg;
	reg [127:0] words;
	reg [1:0] state;
	reg [8:0] line;
	
	assign lcd = lcd_words;
	assign lcden = lcden_reg;
	assign lcdrs = lcdrs_reg;
	assign lcdrw = lcdrw_reg;
	
	parameter
		hello_word0 = "HELLO DSL       ",
		hello_word1 = "LDS OLLEH       ",
		idnum_word0 = "B00902098       ",
		idnum_word1 = "B00902100       ",
		idnum_word2 = "B00902050       ",
		idnum_word3 = "B00902054       ",
		idnum_word4 = "B00902048       ",
		nomem_word  = "No this member. ",
		black_word  = {16{8'b11111111}} ,
		
		reset_state = 2'b00,
		cleared_state = 2'b01,
		moved_state = 2'b10,
		displayed_state = 2'b11;
		
initial 
begin 
			pre_sw1 = 3'b000;
			pre_sw2 = 3'b000;
			
end

always @(posedge clk)
begin
	/*if(button[0] != 1'b1)
	begin
			;//msg_cnt=0;
	end
	else
	begin*/
		if(sw1 != pre_sw1 || (sw1 == 0 && sw2 != pre_sw2))
			begin
				reset();
				set_words();
				pre_sw1 = sw1;
				pre_sw2 = sw2;
		end
		else if(state == reset_state) set_words();
		
		case(sw1)
			3'b000:	display_process(3'b100,	7'b0000000, 16, 1000000000);
			3'b100: display_process(3'b100, 7'b0000000, 16, 1000000000);
			3'b010: display_process(3'b100, 7'b1000000, 16, 1000000000);
			3'b110:	
				case(state)
						reset_state: clean_screen(3'b100);
				endcase
			3'b001:	
			begin
				case(line)
					0: display_process(3'b100, 7'b0000000, 16, 1000000);
					1:
						begin
								case(state)
									reset_state:	move(7'b1000000);
									moved_state:	print(1250000,16);
								endcase
								
						end
				endcase
			end
			3'b101:	if(line < 16)  display_process(3'b111, line[6:0],  0,  50000000);
			3'b011:	if(line < 16)	display_process(3'b111, {3'b100,4'b1111-line[3:0]}, 0, 50000000);
			3'b111:	
			begin
				if(line < 256)	
				begin
					display_process(3'b100, 7'b1000000, 16, 25000000);
				end
			end
		endcase
		t = t + 1;
	//end

end

task set_words;
begin
	case(sw1)
	3'b000:	
		case(sw2)
			3'b000:	words = idnum_word0;
			3'b100:	words = idnum_word1;
			3'b010:	words = idnum_word2;
			3'b110:	words = idnum_word3;
			3'b001:	words = idnum_word4;
			default: words = nomem_word;
		endcase
	3'b100:	words = hello_word0;
	3'b010:	words = hello_word1;
	3'b110:	/*nothing*/;
	3'b001:	words = black_word;
	3'b101:	/*move the cursor from left to right*/;
	3'b011:	/*move the cursor from right to left*/;
	3'b111:	words = {"      0x",hexdit(line>>4),hexdit(line&9'b000001111),"      "};
	endcase
end
endtask

function [7:0] hexdit;
input [3:0] number;

begin
case(number)
0: hexdit = "0";	
1:	hexdit = "1";
2:	hexdit = "2";
3:	hexdit = "3";
4:	hexdit = "4";
5:	hexdit = "5";
6:	hexdit = "6";
7:	hexdit = "7";
8:	hexdit = "8";
9:	hexdit = "9";
10: hexdit = "A";
11: hexdit = "B";
12: hexdit = "C";
13: hexdit = "D";
14: hexdit = "E";
15: hexdit = "F";
endcase
end
endfunction


task display_process;
input [2:0] cursor;
input [6:0] address;
input [7:0]	length;
input [31:0] sleep;

begin
	case(state)
		reset_state: clean_screen(cursor);
		cleared_state:	move(address);
		moved_state:	print(1250000,length);
		displayed_state:	take_rest(sleep);
	endcase
end
endtask

task move;
input [6:0]address;
begin
	case(t)
	28'd50000:	lcd_words = 8'b00000010;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000:	lcd_words = {1'b1, address};
	28'd180000: lcden_reg = 1'b1;
	28'd190000: lcden_reg = 1'b0;
	28'd200000:	
		begin
			state = moved_state;
			t = 0;
		end
	endcase
end
endtask
task reset;
begin
	state = reset_state;
	lcden_reg = 1'b0;
	lcdrs_reg = 1'b0;
	lcdrw_reg = 1'b0;
	pos = 7'b0000000;
	t  = 0;
	line = 0;
end
endtask

task clean_screen;
input [2:0] cursor;

begin
case(t)
	28'd50000:	lcd_words = 8'b00000001;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000: lcd_words = {5'b00001,cursor};
	28'd180000:	lcden_reg = 1'b1;
	28'd190000:	lcden_reg = 1'b0;
	28'd200000: 
		begin
			state = cleared_state;
			t = 0;
		end
endcase
end
endtask

task print;
input [27:0] delay;
input [7:0] length;
	
begin
	if(t == delay)
	begin
		t = 0;
	end
	else 
	begin
		if(pos < length)
		begin
			case(t)
				28'd50000: lcdrs_reg = 1'b1;
				28'd60000: 
					begin
					lcd_words = words[127:120];
					words = words << 8;
					end
				28'd70000: lcden_reg = 1'b1;
				28'd80000:
				begin
					lcden_reg = 1'b0;
					pos = pos + 1;
				end
			endcase
		end
		
		else
		begin
			state = displayed_state;
			lcdrs_reg = 1'b0;
			//pos = 0;
			t = 0;
		end
	end
end
endtask

task take_rest;
input [31:0] sleep;
begin
	if(t == sleep)
	begin
		state = reset_state;
		pos = 0;
		t = 0;
		line = line + 1;
	end
end
endtask
endmodule

module function4(clk, channel, sw1, sw2, sw3, sw4, lcd, lcden, lcdrs, lcdrw, o_gpio);
input clk, channel;
input [2:0] sw1, sw2, sw3, sw4;
output [7:0] lcd;
output lcden, lcdrs, lcdrw;
output [1:0] o_gpio;//ch1: GPIO[0], ch2: GPIO[1]
wire [1:0] o_gpio;
reg [1:0] f_out;
reg [12:0] ch1_fq_clk, ch2_fq_clk;
reg [6:0] ch1_duty_high_value, ch1_duty_low_value, ch2_duty_high_value, ch2_duty_low_value;
reg [18:0] ch1_count, ch2_count;
wire [18:0] ch1_high_clk, ch1_low_clk, ch2_high_clk, ch2_low_clk;
reg [7:0] lcd_words;
reg [31:0] t;
reg [7:0] pos;
reg [2:0] pre_sw1, pre_sw2, pre_sw3, pre_sw4;
reg lcden_reg, lcdrs_reg, lcdrw_reg;
reg [127:0] words, words2;
reg [1:0] state;
reg [8:0] line;
reg prechannel;
assign o_gpio = f_out;
assign lcd = lcd_words;
assign lcden = lcden_reg;
assign lcdrs = lcdrs_reg;
assign lcdrw = lcdrw_reg;
assign ch1_high_clk = ch1_fq_clk * ch1_duty_high_value;
assign ch1_low_clk = ch1_fq_clk * ch1_duty_low_value;
assign ch2_high_clk = ch2_fq_clk * ch2_duty_high_value;
assign ch2_low_clk = ch2_fq_clk * ch2_duty_low_value;

parameter
	f_word0 = "FREQUENCY:100Hz ",
	f_word1 = "FREQUENCY:200Hz ",
	f_word2 = "FREQUENCY:400Hz ",
	f_word3 = "FREQUENCY:800Hz ",
	f_word4 = "FREQUENCY:1600Hz",
	f_word5 = "FREQUENCY:3200Hz",
	f_word6 = "FREQUENCY:6400Hz",
	f_word7 = "FREQUENCY12800Hz",

	d_word0 = "DUTY CYCLE:10%  ",
	d_word1 = "DUTY CYCLE:20%  ",
	d_word2 = "DUTY CYCLE:30%  ",
	d_word3 = "DUTY CYCLE:40%  ",
	d_word4 = "DUTY CYCLE:50%  ",
	d_word5 = "DUTY CYCLE:60%  ",
	d_word6 = "DUTY CYCLE:70%  ",
	d_word7 = "DUTY CYCLE:80%  ",

	reset_state = 2'b00,
	cleared_state = 2'b01,
	moved_state = 2'b10,
	displayed_state = 2'b11;
                
initial 
begin 
	pre_sw1 = 3'b000;
	pre_sw2 = 3'b000;
	ch1_count = 0;
	ch2_count = 0;
	f_out = 2'b11;//high
	prechannel = 1'b0;
end

/*clock frequency = 50MHz, so 1 clock = 0.00000002 sec*/
always @ (sw1)//show frequency information
begin
	if(sw1==3'b000) //100 Hz, 0.01 sec = 500000 clock
		ch1_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
	else if(sw1==3'b001) //200 Hz, 250000 clock
		ch1_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
	else if(sw1==3'b010) //400 Hz, 125000 clock
		ch1_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
	else if(sw1==3'b011) //800 Hz, 62500 clock
		ch1_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
	else if(sw1==3'b100) //1600 Hz, 31250 clock
		ch1_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
	else if(sw1==3'b101) //3200 Hz, 15625 clock
		ch1_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
	else if(sw1==3'b110) //6400 Hz, 7812.5 clock
		ch1_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
	else if(sw1==3'b111) //12800 Hz, 3906.25 clock
		ch1_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (sw3)//show frequency information
begin
	if(sw3==3'b000) //100 Hz, 0.01 sec = 500000 clock
		ch2_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
	else if(sw3==3'b001) //200 Hz, 250000 clock
		ch2_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
	else if(sw3==3'b010) //400 Hz, 125000 clock
		ch2_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
	else if(sw3==3'b011) //800 Hz, 62500 clock
		ch2_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
	else if(sw3==3'b100) //1600 Hz, 31250 clock
		ch2_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
	else if(sw3==3'b101) //3200 Hz, 15625 clock
		ch2_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
	else if(sw3==3'b110) //6400 Hz, 7812.5 clock
		ch2_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
	else if(sw3==3'b111) //12800 Hz, 3906.25 clock
		ch2_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (sw2)//show duty information
begin
	if(sw2==3'b000) begin//duty 10%
		ch1_duty_high_value = 7'd10;
		ch1_duty_low_value = 7'd90;
	end
	else if(sw2==3'b001) begin//duty 20%
		ch1_duty_high_value = 7'd20;
		ch1_duty_low_value = 7'd80;
	end
	else if(sw2==3'b010) begin//duty 30%
		ch1_duty_high_value = 7'd30;
		ch1_duty_low_value = 7'd70;
	end
	else if(sw2==3'b011) begin//duty 40%
		ch1_duty_high_value = 7'd40;
		ch1_duty_low_value = 7'd60;
	end
	else if(sw2==3'b100) begin//duty 50%
		ch1_duty_high_value = 7'd50;
		ch1_duty_low_value = 7'd50;
	end
	else if(sw2==3'b101) begin//duty 60%
		ch1_duty_high_value = 7'd60;
		ch1_duty_low_value = 7'd40;
	end
	else if(sw2==3'b110) begin//duty 70%
		ch1_duty_high_value = 7'd70;
		ch1_duty_low_value = 7'd30;
	end
	else if(sw2==3'b111) begin//duty 80%
		ch1_duty_high_value = 7'd80;
		ch1_duty_low_value = 7'd20;
	end
end

always @ (sw4)//show duty information
begin
	if(sw4==3'b000) begin//duty 10%
		ch2_duty_high_value = 7'd10;
		ch2_duty_low_value = 7'd90;
	end
	else if(sw4==3'b001) begin//duty 20%
		ch2_duty_high_value = 7'd20;
		ch2_duty_low_value = 7'd80;
	end
	else if(sw4==3'b010) begin//duty 30%
		ch2_duty_high_value = 7'd30;
		ch2_duty_low_value = 7'd70;
	end
	else if(sw4==3'b011) begin//duty 40%
		ch2_duty_high_value = 7'd40;
		ch2_duty_low_value = 7'd60;
	end
	else if(sw4==3'b100) begin//duty 50%
		ch2_duty_high_value = 7'd50;
		ch2_duty_low_value = 7'd50;
	end
	else if(sw4==3'b101) begin//duty 60%
		ch2_duty_high_value = 7'd60;
		ch2_duty_low_value = 7'd40;
	end
	else if(sw4==3'b110) begin//duty 70%
		ch2_duty_high_value = 7'd70;
		ch2_duty_low_value = 7'd30;
	end
	else if(sw4==3'b111) begin//duty 80%
		ch2_duty_high_value = 7'd80;
		ch2_duty_low_value = 7'd20;
	end
end

always @(posedge clk)
begin
	/*change the wave of ch1 to high or low*/
	if(ch1_count==ch1_high_clk && f_out[0]==1 || ch1_count==ch1_low_clk && f_out[0]==0)
	begin
		ch1_count = 0;
		f_out[0] = ~f_out[0];
	end
	else ch1_count = ch1_count + 1;
	/*change the wave of ch2 to high or low*/
	if(ch2_count==ch2_high_clk && f_out[1]==1 || ch2_count==ch2_low_clk && f_out[1]==0)
	begin
		ch2_count = 0;
		f_out[1] = ~f_out[1];
	end
	else ch2_count = ch2_count + 1;

	if((sw1 != pre_sw1) || (sw2 != pre_sw2) || (sw3 != pre_sw3) || (sw4 != pre_sw4) || (prechannel != channel))
	begin
		reset();
		set_words();
		prechannel = channel;
		pre_sw1 = sw1;
		pre_sw2 = sw2;
		pre_sw3 = sw3;
		pre_sw4 = sw4;
	end
	else if(state == reset_state) set_words();
   /*if(channel != prechannel)	
	begin
	prechannel = channel;
	set_words();
	end*/
	case(line)
		0: display_process(3'b100, 7'b0000000, 16, 10000);//1000000000
		1:
		begin
			case(state)
				reset_state:    move(7'b1000000);
				moved_state:    print(1250000,16);
			endcase
		end
	endcase
	t = t + 1;
end

task set_words;
begin

	if(channel == 1'b0)
	begin
        case(sw1)
        0:words = f_word0;
        1:words = f_word1;
        2:words = f_word2;
        3:words = f_word3;
        4:words = f_word4;
        5:words = f_word5;
        6:words = f_word6;
        7:words = f_word7;
        endcase
        
        case(sw2)
        0:words2 = d_word0;
        1:words2 = d_word1;
        2:words2 = d_word2;
        3:words2 = d_word3;
        4:words2 = d_word4;
        5:words2 = d_word5;
        6:words2 = d_word6;
        7:words2 = d_word7;
        endcase
	end
	else if(channel == 1'b1)
	begin
        case(sw3)
        0:words = f_word0;
        1:words = f_word1;
        2:words = f_word2;
        3:words = f_word3;
        4:words = f_word4;
        5:words = f_word5;
        6:words = f_word6;
        7:words = f_word7;
        endcase
        
        case(sw4)
        0:words2 = d_word0;
        1:words2 = d_word1;
        2:words2 = d_word2;
        3:words2 = d_word3;
        4:words2 = d_word4;
        5:words2 = d_word5;
        6:words2 = d_word6;
        7:words2 = d_word7;
        endcase
	end
end
endtask

task display_process;
input [2:0] cursor;
input [6:0] address;
input [7:0] length;
input [31:0] sleep;

begin
	case(state)
		reset_state: clean_screen(cursor);
		cleared_state:  move(address);
		moved_state:    print(1250000,length);
		displayed_state:        take_rest(sleep);
	endcase
end
endtask

task move;
input [6:0]address;
begin
	case(t)
		28'd50000:      lcd_words = 8'b00000010;
		28'd60000:      lcden_reg = 1'b1;
		28'd70000:      lcden_reg = 1'b0;
		28'd170000:     lcd_words = {1'b1, address};
		28'd180000: lcden_reg = 1'b1;
		28'd190000: lcden_reg = 1'b0;
		28'd200000:     
			begin
				state = moved_state;
				t = 0;
			end
	endcase
end
endtask

task reset;
begin
        state = reset_state;
        lcden_reg = 1'b0;
        lcdrs_reg = 1'b0;
        lcdrw_reg = 1'b0;
        pos = 7'b0000000;
        t  = 0;
        line = 0;
end
endtask

task clean_screen;
input [2:0] cursor;
begin
	case(t)
		28'd50000:      lcd_words = 8'b00000001;
		28'd60000:      lcden_reg = 1'b1;
		28'd70000:      lcden_reg = 1'b0;
		28'd170000: lcd_words = {5'b00001,cursor};
		28'd180000:     lcden_reg = 1'b1;
		28'd190000:     lcden_reg = 1'b0;
		28'd200000: 
                begin
                        state = cleared_state;
                        t = 0;
                end
	endcase
end
endtask

task print;
input [27:0] delay;
input [7:0] length;
        
begin
	if(t == delay)
	begin
		t = 0;
	end
	else begin
		if(pos < length)
		begin
			case(t)
				28'd50000: lcdrs_reg = 1'b1;
				28'd60000: 
						begin
							case(line)
								0:
									begin
										lcd_words = words[127:120];
										words = words << 8;
									end
								1:
									begin
										lcd_words = words2[127:120];
										words2 = words2 << 8;
									end
							endcase
						end
				28'd70000: lcden_reg = 1'b1;
				28'd80000:
						begin
							lcden_reg = 1'b0;
							pos = pos + 1;
						end
			endcase
		end
		else begin
			state = displayed_state;
			lcdrs_reg = 1'b0;
			//pos = 0;
			t = 0;
		end
	end
end
endtask

task take_rest;
input [31:0] sleep;
begin
	if(t == sleep)
	begin
		state = reset_state;
		pos = 0;
		t = 0;
		line = line + 1;
	end
end
endtask
endmodule


module f5(f_clk,ch_choose,ch1_fq,ch1_duty,ch2_fq,ch2_duty,o_gpio,fq1,fq2,fq3,fq4,fq5,duty1,duty2);
input f_clk;
input ch_choose;//use sw[0] to control
input [2:0] ch1_fq, ch1_duty, ch2_fq, ch2_duty;
/*ch1_fq: sw[17][16][15], ch1_duty: sw[14][13][12], ch2_fq: sw[11][10][9], ch2_duty: sw[8][7][6]*/
output [6:0] fq1,fq2,fq3,fq4,fq5,duty1,duty2;
output [1:0] o_gpio;//o_ch1: GPIO[0], o_ch2: GPIO[1]
wire [1:0] o_gpio;
reg [1:0] f_out;
reg [6:0] fq1_latch,fq2_latch,fq3_latch,fq4_latch,fq5_latch,duty1_latch,duty2_latch;
reg [12:0] ch1_fq_clk, ch2_fq_clk;
reg [6:0] ch1_duty_high_value, ch1_duty_low_value, ch2_duty_high_value, ch2_duty_low_value;
reg [18:0] ch1_count, ch2_count;
wire [18:0] ch1_high_clk, ch1_low_clk, ch2_high_clk, ch2_low_clk;

assign o_gpio = f_out;
assign fq1 = fq1_latch;//1-digit for frequency
assign fq2 = fq2_latch;//10-digit for frequency
assign fq3 = fq3_latch;//100-digit for frequency
assign fq4 = fq4_latch;//1000-digit for frequency
assign fq5 = fq5_latch;//10000-digit for frequency
assign duty1 = duty1_latch;//1-digit for duty cycle
assign duty2 = duty2_latch;//10-digit for duty cycle
assign ch1_high_clk = ch1_fq_clk * ch1_duty_high_value;
assign ch1_low_clk = ch1_fq_clk * ch1_duty_low_value;
assign ch2_high_clk = ch2_fq_clk * ch2_duty_high_value;
assign ch2_low_clk = ch2_fq_clk * ch2_duty_low_value;

initial
        begin
                ch1_count = 0;
                ch2_count = 0;
                f_out = 2'b11;//high
                fq1_latch = 7'b1000000;//0
                fq2_latch = 7'b1000000;//0
                fq3_latch = 7'b1111111;//dark
                fq4_latch = 7'b1111111;//dark
                fq5_latch = 7'b1111111;//dark
                duty1_latch = 7'b1000000;//0
                duty2_latch = 7'b1111111;//dark
        end
/*clock frequency = 50MHz, so 1 clock = 0.00000002 sec*/
always @ (ch1_fq)//show frequency information
begin
        if(ch1_fq==3'b000) //100 Hz, 0.01 sec = 500000 clock
                ch1_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
        else if(ch1_fq==3'b001) //200 Hz, 250000 clock
                ch1_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
        else if(ch1_fq==3'b010) //400 Hz, 125000 clock
                ch1_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
        else if(ch1_fq==3'b011) //800 Hz, 62500 clock
                ch1_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
        else if(ch1_fq==3'b100) //1600 Hz, 31250 clock
                ch1_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
        else if(ch1_fq==3'b101) //3200 Hz, 15625 clock
                ch1_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
        else if(ch1_fq==3'b110) //6400 Hz, 7812.5 clock
                ch1_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
        else if(ch1_fq==3'b111) //12800 Hz, 3906.25 clock
                ch1_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (ch2_fq)//show frequency information
begin
        if(ch2_fq==3'b000) //100 Hz, 0.01 sec = 500000 clock
                ch2_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
        else if(ch2_fq==3'b001) //200 Hz, 250000 clock
                ch2_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
        else if(ch2_fq==3'b010) //400 Hz, 125000 clock
                ch2_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
        else if(ch2_fq==3'b011) //800 Hz, 62500 clock
                ch2_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
        else if(ch2_fq==3'b100) //1600 Hz, 31250 clock
                ch2_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
        else if(ch2_fq==3'b101) //3200 Hz, 15625 clock
                ch2_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
        else if(ch2_fq==3'b110) //6400 Hz, 7812.5 clock
                ch2_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
        else if(ch2_fq==3'b111) //12800 Hz, 3906.25 clock
                ch2_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (ch1_duty)//show duty information
begin
        if(ch1_duty==3'b000) begin//duty 10%
                ch1_duty_high_value <= 7'd10;
                ch1_duty_low_value <= 7'd90;
        end
        else if(ch1_duty==3'b001) begin//duty 20%
                ch1_duty_high_value <= 7'd20;
                ch1_duty_low_value <= 7'd80;
        end
        else if(ch1_duty==3'b010) begin//duty 30%
                ch1_duty_high_value <= 7'd30;
                ch1_duty_low_value <= 7'd70;
        end
        else if(ch1_duty==3'b011) begin//duty 40%
                ch1_duty_high_value <= 7'd40;
                ch1_duty_low_value <= 7'd60;
        end
        else if(ch1_duty==3'b100) begin//duty 50%
                ch1_duty_high_value <= 7'd50;
                ch1_duty_low_value <= 7'd50;
        end
        else if(ch1_duty==3'b101) begin//duty 60%
                ch1_duty_high_value <= 7'd60;
                ch1_duty_low_value <= 7'd40;
        end
        else if(ch1_duty==3'b110) begin//duty 70%
                ch1_duty_high_value <= 7'd70;
                ch1_duty_low_value <= 7'd30;
        end
        else if(ch1_duty==3'b111) begin//duty 80%
                ch1_duty_high_value <= 7'd80;
                ch1_duty_low_value <= 7'd20;
        end
end

always @ (ch2_duty)//show duty information
begin
        if(ch2_duty==3'b000) begin//duty 10%
                ch2_duty_high_value <= 7'd10;
                ch2_duty_low_value <= 7'd90;
        end
        else if(ch2_duty==3'b001) begin//duty 20%
                ch2_duty_high_value <= 7'd20;
                ch2_duty_low_value <= 7'd80;
        end
        else if(ch2_duty==3'b010) begin//duty 30%
                ch2_duty_high_value <= 7'd30;
                ch2_duty_low_value <= 7'd70;
        end
        else if(ch2_duty==3'b011) begin//duty 40%
                ch2_duty_high_value <= 7'd40;
                ch2_duty_low_value <= 7'd60;
        end
        else if(ch2_duty==3'b100) begin//duty 50%
                ch2_duty_high_value <= 7'd50;
                ch2_duty_low_value <= 7'd50;
        end
        else if(ch2_duty==3'b101) begin//duty 60%
                ch2_duty_high_value <= 7'd60;
                ch2_duty_low_value <= 7'd40;
        end
        else if(ch2_duty==3'b110) begin//duty 70%
                ch2_duty_high_value <= 7'd70;
                ch2_duty_low_value <= 7'd30;
        end
        else if(ch2_duty==3'b111) begin//duty 80%
                ch2_duty_high_value <= 7'd80;
                ch2_duty_low_value <= 7'd20;
        end
end

always @ (ch_choose)//choose which channel to show on 7-segment led
begin
        if(ch_choose==0) begin//show information for ch1
                /*show frequency*/
                if(ch1_fq==3'b000) begin//100 Hz
                        fq3_latch <= 7'b1111001;//1
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b001) begin//200 Hz
                        fq3_latch <= 7'b0100100;//2
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b010) begin//400 Hz
                        fq3_latch <= 7'b0011001;//4
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b011) begin//800 Hz
                        fq3_latch <= 7'b0000000;//8
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b100) begin//1600 Hz
                        fq3_latch <= 7'b0000010;//6
                        fq4_latch <= 7'b1111001;//1
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b101) begin//3200 Hz
                        fq3_latch <= 7'b0100100;//2
                        fq4_latch <= 7'b0110000;//3
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b110) begin//6400 Hz
                        fq3_latch <= 7'b0011001;//4
                        fq4_latch <= 7'b0000010;//6
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch1_fq==3'b111) begin//12800 Hz
                        fq3_latch <= 7'b0000000;//8
                        fq4_latch <= 7'b0100100;//2
                        fq5_latch <= 7'b1111001;//1
                end
                /*show duty cycle*/
                if(ch1_duty==3'b000) duty2_latch <= 7'b1111001;//duty 10%
                else if(ch1_duty==3'b001) duty2_latch = 7'b0100100;//duty 20%
                else if(ch1_duty==3'b010) duty2_latch = 7'b0110000;//duty 30%
                else if(ch1_duty==3'b011) duty2_latch = 7'b0011001;//duty 40%
                else if(ch1_duty==3'b100) duty2_latch = 7'b0010010;//duty 50%
                else if(ch1_duty==3'b101) duty2_latch = 7'b0000010;//duty 60%
                else if(ch1_duty==3'b110) duty2_latch = 7'b1111000;//duty 70%
                else if(ch1_duty==3'b111) duty2_latch = 7'b0000000;//duty 80%
        end
        else begin//show information for ch2
                /*show frequency*/
                if(ch2_fq==3'b000) begin//100 Hz
                        fq3_latch <= 7'b1111001;//1
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b001) begin//200 Hz
                        fq3_latch <= 7'b0100100;//2
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b010) begin//400 Hz
                        fq3_latch <= 7'b0011001;//4
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b011) begin//800 Hz
                        fq3_latch <= 7'b0000000;//8
                        fq4_latch <= 7'b1111111;//dark
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b100) begin//1600 Hz
                        fq3_latch <= 7'b0000010;//6
                        fq4_latch <= 7'b1111001;//1
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b101) begin//3200 Hz
                        fq3_latch <= 7'b0100100;//2
                        fq4_latch <= 7'b0110000;//3
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b110) begin//6400 Hz
                        fq3_latch <= 7'b0011001;//4
                        fq4_latch <= 7'b0000010;//6
                        fq5_latch <= 7'b1111111;//dark
                end
                else if(ch2_fq==3'b111) begin//12800 Hz
                        fq3_latch <= 7'b0000000;//8
                        fq4_latch <= 7'b0100100;//2
                        fq5_latch <= 7'b1111001;//1
                end
                /*show duty cycle*/
                if(ch2_duty==3'b000) duty2_latch = 7'b1111001;//duty 10%
                else if(ch2_duty==3'b001) duty2_latch = 7'b0100100;//duty 20%
                else if(ch2_duty==3'b010) duty2_latch = 7'b0110000;//duty 30%
                else if(ch2_duty==3'b011) duty2_latch = 7'b0011001;//duty 40%
                else if(ch2_duty==3'b100) duty2_latch = 7'b0010010;//duty 50%
                else if(ch2_duty==3'b101) duty2_latch = 7'b0000010;//duty 60%
                else if(ch2_duty==3'b110) duty2_latch = 7'b1111000;//duty 70%
                else if(ch2_duty==3'b111) duty2_latch = 7'b0000000;//duty 80%
        end
end

always @ (posedge f_clk)//set frequency by clock
begin
        /*change the wave of ch1 to high or low*/
        if(ch1_count==ch1_high_clk && f_out[0]==1 || ch1_count==ch1_low_clk && f_out[0]==0)
        begin
                ch1_count = 0;
                f_out[0] = ~f_out[0];
        end
        else ch1_count = ch1_count + 1;
        /*change the wave of ch2 to high or low*/
        if(ch2_count==ch2_high_clk && f_out[1]==1 || ch2_count==ch2_low_clk && f_out[1]==0)
        begin
                ch2_count = 0;
                f_out[1] = ~f_out[1];
        end
        else ch2_count = ch2_count + 1;
end

endmodule
