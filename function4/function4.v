module function4(clk, channel, sw1, sw2, sw3, sw4, lcd, lcden, lcdrs, lcdrw, o_gpio);
input clk, channel;
input [2:0] sw1, sw2, sw3, sw4;
output [7:0] lcd;
output lcden, lcdrs, lcdrw;
output [1:0] o_gpio;//ch1: GPIO[0], ch2: GPIO[1]
wire [1:0] o_gpio;
reg [1:0] f_out;
reg [12:0] ch1_fq_clk, ch2_fq_clk;
reg [6:0] ch1_duty_high_value, ch1_duty_low_value, ch2_duty_high_value, ch2_duty_low_value;
reg [18:0] ch1_count, ch2_count;
wire [18:0] ch1_high_clk, ch1_low_clk, ch2_high_clk, ch2_low_clk;
reg [7:0] lcd_words;
reg [31:0] t;
reg [7:0] pos;
reg [2:0] pre_sw1, pre_sw2, pre_sw3, pre_sw4;
reg lcden_reg, lcdrs_reg, lcdrw_reg;
reg [127:0] words, words2;
reg [1:0] state;
reg [8:0] line;
reg prechannel;
assign o_gpio = f_out;
assign lcd = lcd_words;
assign lcden = lcden_reg;
assign lcdrs = lcdrs_reg;
assign lcdrw = lcdrw_reg;
assign ch1_high_clk = ch1_fq_clk * ch1_duty_high_value;
assign ch1_low_clk = ch1_fq_clk * ch1_duty_low_value;
assign ch2_high_clk = ch2_fq_clk * ch2_duty_high_value;
assign ch2_low_clk = ch2_fq_clk * ch2_duty_low_value;

parameter
	f_word0 = "FREQUENCY:100Hz ",
	f_word1 = "FREQUENCY:200Hz ",
	f_word2 = "FREQUENCY:400Hz ",
	f_word3 = "FREQUENCY:800Hz ",
	f_word4 = "FREQUENCY:1600Hz",
	f_word5 = "FREQUENCY:3200Hz",
	f_word6 = "FREQUENCY:6400Hz",
	f_word7 = "FREQUENCY12800Hz",

	d_word0 = "DUTY CYCLE:10%  ",
	d_word1 = "DUTY CYCLE:20%  ",
	d_word2 = "DUTY CYCLE:30%  ",
	d_word3 = "DUTY CYCLE:40%  ",
	d_word4 = "DUTY CYCLE:50%  ",
	d_word5 = "DUTY CYCLE:60%  ",
	d_word6 = "DUTY CYCLE:70%  ",
	d_word7 = "DUTY CYCLE:80%  ",

	reset_state = 2'b00,
	cleared_state = 2'b01,
	moved_state = 2'b10,
	displayed_state = 2'b11;
                
initial 
begin 
	pre_sw1 = 3'b000;
	pre_sw2 = 3'b000;
	ch1_count = 0;
	ch2_count = 0;
	f_out = 2'b11;//high
	prechannel = 1'b0;
end

/*clock frequency = 50MHz, so 1 clock = 0.00000002 sec*/
always @ (sw1)//show frequency information
begin
	if(sw1==3'b000) //100 Hz, 0.01 sec = 500000 clock
		ch1_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
	else if(sw1==3'b001) //200 Hz, 250000 clock
		ch1_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
	else if(sw1==3'b010) //400 Hz, 125000 clock
		ch1_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
	else if(sw1==3'b011) //800 Hz, 62500 clock
		ch1_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
	else if(sw1==3'b100) //1600 Hz, 31250 clock
		ch1_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
	else if(sw1==3'b101) //3200 Hz, 15625 clock
		ch1_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
	else if(sw1==3'b110) //6400 Hz, 7812.5 clock
		ch1_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
	else if(sw1==3'b111) //12800 Hz, 3906.25 clock
		ch1_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (sw3)//show frequency information
begin
	if(sw3==3'b000) //100 Hz, 0.01 sec = 500000 clock
		ch2_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
	else if(sw3==3'b001) //200 Hz, 250000 clock
		ch2_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
	else if(sw3==3'b010) //400 Hz, 125000 clock
		ch2_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
	else if(sw3==3'b011) //800 Hz, 62500 clock
		ch2_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
	else if(sw3==3'b100) //1600 Hz, 31250 clock
		ch2_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
	else if(sw3==3'b101) //3200 Hz, 15625 clock
		ch2_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
	else if(sw3==3'b110) //6400 Hz, 7812.5 clock
		ch2_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
	else if(sw3==3'b111) //12800 Hz, 3906.25 clock
		ch2_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (sw2)//show duty information
begin
	if(sw2==3'b000) begin//duty 10%
		ch1_duty_high_value = 7'd10;
		ch1_duty_low_value = 7'd90;
	end
	else if(sw2==3'b001) begin//duty 20%
		ch1_duty_high_value = 7'd20;
		ch1_duty_low_value = 7'd80;
	end
	else if(sw2==3'b010) begin//duty 30%
		ch1_duty_high_value = 7'd30;
		ch1_duty_low_value = 7'd70;
	end
	else if(sw2==3'b011) begin//duty 40%
		ch1_duty_high_value = 7'd40;
		ch1_duty_low_value = 7'd60;
	end
	else if(sw2==3'b100) begin//duty 50%
		ch1_duty_high_value = 7'd50;
		ch1_duty_low_value = 7'd50;
	end
	else if(sw2==3'b101) begin//duty 60%
		ch1_duty_high_value = 7'd60;
		ch1_duty_low_value = 7'd40;
	end
	else if(sw2==3'b110) begin//duty 70%
		ch1_duty_high_value = 7'd70;
		ch1_duty_low_value = 7'd30;
	end
	else if(sw2==3'b111) begin//duty 80%
		ch1_duty_high_value = 7'd80;
		ch1_duty_low_value = 7'd20;
	end
end

always @ (sw4)//show duty information
begin
	if(sw4==3'b000) begin//duty 10%
		ch2_duty_high_value = 7'd10;
		ch2_duty_low_value = 7'd90;
	end
	else if(sw4==3'b001) begin//duty 20%
		ch2_duty_high_value = 7'd20;
		ch2_duty_low_value = 7'd80;
	end
	else if(sw4==3'b010) begin//duty 30%
		ch2_duty_high_value = 7'd30;
		ch2_duty_low_value = 7'd70;
	end
	else if(sw4==3'b011) begin//duty 40%
		ch2_duty_high_value = 7'd40;
		ch2_duty_low_value = 7'd60;
	end
	else if(sw4==3'b100) begin//duty 50%
		ch2_duty_high_value = 7'd50;
		ch2_duty_low_value = 7'd50;
	end
	else if(sw4==3'b101) begin//duty 60%
		ch2_duty_high_value = 7'd60;
		ch2_duty_low_value = 7'd40;
	end
	else if(sw4==3'b110) begin//duty 70%
		ch2_duty_high_value = 7'd70;
		ch2_duty_low_value = 7'd30;
	end
	else if(sw4==3'b111) begin//duty 80%
		ch2_duty_high_value = 7'd80;
		ch2_duty_low_value = 7'd20;
	end
end

always @(posedge clk)
begin
	/*change the wave of ch1 to high or low*/
	if(ch1_count==ch1_high_clk && f_out[0]==1 || ch1_count==ch1_low_clk && f_out[0]==0)
	begin
		ch1_count = 0;
		f_out[0] = ~f_out[0];
	end
	else ch1_count = ch1_count + 1;
	/*change the wave of ch2 to high or low*/
	if(ch2_count==ch2_high_clk && f_out[1]==1 || ch2_count==ch2_low_clk && f_out[1]==0)
	begin
		ch2_count = 0;
		f_out[1] = ~f_out[1];
	end
	else ch2_count = ch2_count + 1;

	if((sw1 != pre_sw1) || (sw2 != pre_sw2) || (sw3 != pre_sw3) || (sw4 != pre_sw4) || (prechannel != channel))
	begin
		reset();
		set_words();
		prechannel = channel;
		pre_sw1 = sw1;
		pre_sw2 = sw2;
		pre_sw3 = sw3;
		pre_sw4 = sw4;
	end
	else if(state == reset_state) set_words();
   /*if(channel != prechannel)	
	begin
	prechannel = channel;
	set_words();
	end*/
	case(line)
		0: display_process(3'b100, 7'b0000000, 16, 10000);//1000000000
		1:
		begin
			case(state)
				reset_state:    move(7'b1000000);
				moved_state:    print(1250000,16);
			endcase
		end
	endcase
	t = t + 1;
end

task moveleft;
begin
	case(t)
		28'd50000:      lcd_words = 8'b00001111;
		28'd60000:      lcden_reg = 1'b1;
		28'd70000:      lcden_reg = 1'b0;
	endcase
end
endtask

task set_words;
begin

	if(channel == 1'b0)
	begin
        case(sw1)
        0:words = f_word0;
        1:words = f_word1;
        2:words = f_word2;
        3:words = f_word3;
        4:words = f_word4;
        5:words = f_word5;
        6:words = f_word6;
        7:words = f_word7;
        endcase
        
        case(sw2)
        0:words2 = d_word0;
        1:words2 = d_word1;
        2:words2 = d_word2;
        3:words2 = d_word3;
        4:words2 = d_word4;
        5:words2 = d_word5;
        6:words2 = d_word6;
        7:words2 = d_word7;
        endcase
	end
	else if(channel == 1'b1)
	begin
        case(sw3)
        0:words = f_word0;
        1:words = f_word1;
        2:words = f_word2;
        3:words = f_word3;
        4:words = f_word4;
        5:words = f_word5;
        6:words = f_word6;
        7:words = f_word7;
        endcase
        
        case(sw4)
        0:words2 = d_word0;
        1:words2 = d_word1;
        2:words2 = d_word2;
        3:words2 = d_word3;
        4:words2 = d_word4;
        5:words2 = d_word5;
        6:words2 = d_word6;
        7:words2 = d_word7;
        endcase
	end
end
endtask

task display_process;
input [2:0] cursor;
input [6:0] address;
input [7:0] length;
input [31:0] sleep;

begin
	case(state)
		reset_state: clean_screen(cursor);
		cleared_state:  move(address);
		moved_state:    print(1250000,length);
		displayed_state:        take_rest(sleep);
	endcase
end
endtask

task move;
input [6:0]address;
begin
	case(t)
		28'd50000:      lcd_words = 8'b00000010;
		28'd60000:      lcden_reg = 1'b1;
		28'd70000:      lcden_reg = 1'b0;
		28'd170000:     lcd_words = {1'b1, address};
		28'd180000: lcden_reg = 1'b1;
		28'd190000: lcden_reg = 1'b0;
		28'd200000:     
			begin
				state = moved_state;
				t = 0;
			end
	endcase
end
endtask

task reset;
begin
        state = reset_state;
        lcden_reg = 1'b0;
        lcdrs_reg = 1'b0;
        lcdrw_reg = 1'b0;
        pos = 7'b0000000;
        t  = 0;
        line = 0;
end
endtask

task clean_screen;
input [2:0] cursor;
begin
	case(t)
		28'd50000:      lcd_words = 8'b00000001;
		28'd60000:      lcden_reg = 1'b1;
		28'd70000:      lcden_reg = 1'b0;
		28'd170000: lcd_words = {5'b00001,cursor};
		28'd180000:     lcden_reg = 1'b1;
		28'd190000:     lcden_reg = 1'b0;
		28'd200000: 
                begin
                        state = cleared_state;
                        t = 0;
                end
	endcase
end
endtask

task print;
input [27:0] delay;
input [7:0] length;
        
begin
	if(t == delay)
	begin
		t = 0;
	end
	else begin
		if(pos < length)
		begin
			case(t)
				28'd50000: lcdrs_reg = 1'b1;
				28'd60000: 
						begin
							case(line)
								0:
									begin
										lcd_words = words[127:120];
										words = words << 8;
									end
								1:
									begin
										lcd_words = words2[127:120];
										words2 = words2 << 8;
									end
							endcase
						end
				28'd70000: lcden_reg = 1'b1;
				28'd80000:
						begin
							lcden_reg = 1'b0;
							pos = pos + 1;
						end
			endcase
		end
		else begin
			state = displayed_state;
			lcdrs_reg = 1'b0;
			//pos = 0;
			t = 0;
		end
	end
end
endtask

task take_rest;
input [31:0] sleep;
begin
	if(t == sleep)
	begin
		state = reset_state;
		pos = 0;
		t = 0;
		line = line + 1;
	end
end
endtask
endmodule
