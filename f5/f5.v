<<<<<<< HEAD
<<<<<<< HEAD
module f5(f_clk,fq,duty,data,fq1,fq2,fq3,fq4,fq5,duty1,duty2,lcd,lcden,lcdrs,lcdrw);
input [2:0] fq, duty;
=======
module f5(f_clk,fq,duty,data,fq1,fq2,fq3,fq4,fq5,duty1,duty2);
>>>>>>> 002d9b5a5c5f6b24948d6b2f42f0df9cf449b7c5
=======
module f5(f_clk,ch_choose,ch1_fq,ch1_duty,ch2_fq,ch2_duty,o_ch1,o_ch2,fq1,fq2,fq3,fq4,fq5,duty1,duty2);
>>>>>>> b129323665fb248d977efe358d0275a874c11f21
input f_clk;
input ch_choose;//use sw[0] to control
input [2:0] ch1_fq, ch1_duty, ch2_fq, ch2_duty;
/*ch1_fq: sw[17][16][15], ch1_duty: sw[14][13][12], ch2_fq: sw[11][10][9], ch2_duty: sw[8][7][6]*/
output [6:0] fq1,fq2,fq3,fq4,fq5,duty1,duty2;
<<<<<<< HEAD
output data;
wire data;
<<<<<<< HEAD

//----------------------------
output [7:0] lcd;
output lcden;
output lcdrw;
output lcdrs;
//----------------------------
//reg [5:0] address;
reg f_out;
reg [6:0] fq1_latch,fq2_latch,fq3_latch,fq4_latch,fq5_latch,duty1_latch,duty2_latch;
reg [7:0] fq_value, duty_value, i;
wire upwave, downwave;
//----------------
reg [7:0] lcd_reg;
reg lcden_reg;
reg lcdrw_reg;
reg lcdrs_reg;
reg [127:0] msg;
reg [127:0]	msg2;
reg [7:0] pos;
reg line;
reg [1:0] state;
//-----------------
=======
reg f_out;
=======
output o_ch1, o_ch2;//o_ch1: GPIO[0], o_ch2: GPIO[1]
wire o_ch1, o_ch2;
reg f_ch1_out, f_ch2_out;
>>>>>>> b129323665fb248d977efe358d0275a874c11f21
reg [6:0] fq1_latch,fq2_latch,fq3_latch,fq4_latch,fq5_latch,duty1_latch,duty2_latch;
reg [12:0] ch1_fq_clk, ch2_fq_clk;
reg [6:0] ch1_duty_high_value, ch1_duty_low_value, ch2_duty_high_value, ch2_duty_low_value;
reg [18:0] ch1_count, ch2_count;
wire [18:0] ch1_high_clk, ch1_low_clk, ch2_high_clk, ch2_low_clk;

<<<<<<< HEAD
>>>>>>> 002d9b5a5c5f6b24948d6b2f42f0df9cf449b7c5
assign data = f_out;
=======
assign o_ch1 = f_ch1_out;
assign o_ch2 = f_ch2_out;
>>>>>>> b129323665fb248d977efe358d0275a874c11f21
assign fq1 = fq1_latch;//1-digit for frequency
assign fq2 = fq2_latch;//10-digit for frequency
assign fq3 = fq3_latch;//100-digit for frequency
assign fq4 = fq4_latch;//1000-digit for frequency
assign fq5 = fq5_latch;//10000-digit for frequency
assign duty1 = duty1_latch;//1-digit for duty cycle
assign duty2 = duty2_latch;//10-digit for duty cycle
<<<<<<< HEAD
<<<<<<< HEAD
assign upwave = fq_value * duty_value;
assign downwave = fq_value * (8'd100 - duty_value);
//---------------
assign lcd = lcd_reg;
assign lcden = lcden_reg;
assign lcdrw = lcdrw_reg;
assign lcdrs = lcdrs_reg;
=======
assign high_clk = fq_clk * duty_high_value;
assign low_clk = fq_clk * duty_low_value;
>>>>>>> 002d9b5a5c5f6b24948d6b2f42f0df9cf449b7c5
=======
assign ch1_high_clk = ch1_fq_clk * ch1_duty_high_value;
assign ch1_low_clk = ch1_fq_clk * ch1_duty_low_value;
assign ch2_high_clk = ch2_fq_clk * ch2_duty_high_value;
assign ch2_low_clk = ch2_fq_clk * ch2_duty_low_value;
>>>>>>> b129323665fb248d977efe358d0275a874c11f21

parameter
		reset_state = 2'b00,
		cleared_state = 2'b01,
		moved_state = 2'b10,
		printed_state = 2'b11;
//---------------
initial
	begin
		ch1_count = 0;
		ch2_count = 0;
		f_ch1_out = 1;//high
		f_ch2_out = 1;//high
		fq1_latch = 7'b1000000;//0
		fq2_latch = 7'b1000000;//0
		fq3_latch = 7'b1111111;//dark
		fq4_latch = 7'b1111111;//dark
		fq5_latch = 7'b1111111;//dark
		duty1_latch = 7'b1000000;//0
		duty2_latch = 7'b1111111;//dark
	end
/*clock frequency = 50MHz, so 1 clock = 0.00000002 sec*/
always @ (ch1_fq)//show frequency information
begin
	if(ch1_fq==3'b000) //100 Hz, 0.01 sec = 500000 clock
		ch1_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
	else if(ch1_fq==3'b001) //200 Hz, 250000 clock
		ch1_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
	else if(ch1_fq==3'b010) //400 Hz, 125000 clock
		ch1_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
	else if(ch1_fq==3'b011) //800 Hz, 62500 clock
		ch1_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
	else if(ch1_fq==3'b100) //1600 Hz, 31250 clock
		ch1_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
	else if(ch1_fq==3'b101) //3200 Hz, 15625 clock
		ch1_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
	else if(ch1_fq==3'b110) //6400 Hz, 7812.5 clock
		ch1_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
	else if(ch1_fq==3'b111) //12800 Hz, 3906.25 clock
		ch1_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (ch2_fq)//show frequency information
begin
	if(ch2_fq==3'b000) //100 Hz, 0.01 sec = 500000 clock
		ch2_fq_clk = 13'd5000;//set clock divided by 100, so 500000/100=5000
	else if(ch2_fq==3'b001) //200 Hz, 250000 clock
		ch2_fq_clk = 13'd2500;//set clock divided by 100, so 250000/100=2500
	else if(ch2_fq==3'b010) //400 Hz, 125000 clock
		ch2_fq_clk = 13'd1250;//set clock divided by 100, so 250000/100=
	else if(ch2_fq==3'b011) //800 Hz, 62500 clock
		ch2_fq_clk = 13'd625;//set clock divided by 100, so 62500/100=625
	else if(ch2_fq==3'b100) //1600 Hz, 31250 clock
		ch2_fq_clk = 13'd312;//set clock divided by 100, so 31250/100=312
	else if(ch2_fq==3'b101) //3200 Hz, 15625 clock
		ch2_fq_clk = 13'd156;//set clock divided by 100, so 15625/100=156
	else if(ch2_fq==3'b110) //6400 Hz, 7812.5 clock
		ch2_fq_clk = 13'd78;//set clock divided by 100, so 7812.5/100=78
	else if(ch2_fq==3'b111) //12800 Hz, 3906.25 clock
		ch2_fq_clk = 13'd39;//set clock divided by 100, so 3906.25/100=39
end

always @ (ch1_duty)//show duty information
begin
	if(ch1_duty==3'b000) begin//duty 10%
		ch1_duty_high_value <= 7'd10;
		ch1_duty_low_value <= 7'd90;
	end
	else if(ch1_duty==3'b001) begin//duty 20%
		ch1_duty_high_value <= 7'd20;
		ch1_duty_low_value <= 7'd80;
	end
	else if(ch1_duty==3'b010) begin//duty 30%
		ch1_duty_high_value <= 7'd30;
		ch1_duty_low_value <= 7'd70;
	end
	else if(ch1_duty==3'b011) begin//duty 40%
		ch1_duty_high_value <= 7'd40;
		ch1_duty_low_value <= 7'd60;
	end
	else if(ch1_duty==3'b100) begin//duty 50%
		ch1_duty_high_value <= 7'd50;
		ch1_duty_low_value <= 7'd50;
	end
	else if(ch1_duty==3'b101) begin//duty 60%
		ch1_duty_high_value <= 7'd60;
		ch1_duty_low_value <= 7'd40;
	end
	else if(ch1_duty==3'b110) begin//duty 70%
		ch1_duty_high_value <= 7'd70;
		ch1_duty_low_value <= 7'd30;
	end
	else if(ch1_duty==3'b111) begin//duty 80%
		ch1_duty_high_value <= 7'd80;
		ch1_duty_low_value <= 7'd20;
	end
end

always @ (ch2_duty)//show duty information
begin
	if(ch2_duty==3'b000) begin//duty 10%
		ch2_duty_high_value <= 7'd10;
		ch2_duty_low_value <= 7'd90;
	end
	else if(ch2_duty==3'b001) begin//duty 20%
		ch2_duty_high_value <= 7'd20;
		ch2_duty_low_value <= 7'd80;
	end
	else if(ch2_duty==3'b010) begin//duty 30%
		ch2_duty_high_value <= 7'd30;
		ch2_duty_low_value <= 7'd70;
	end
	else if(ch2_duty==3'b011) begin//duty 40%
		ch2_duty_high_value <= 7'd40;
		ch2_duty_low_value <= 7'd60;
	end
	else if(ch2_duty==3'b100) begin//duty 50%
		ch2_duty_high_value <= 7'd50;
		ch2_duty_low_value <= 7'd50;
	end
	else if(ch2_duty==3'b101) begin//duty 60%
		ch2_duty_high_value <= 7'd60;
		ch2_duty_low_value <= 7'd40;
	end
	else if(ch2_duty==3'b110) begin//duty 70%
		ch2_duty_high_value <= 7'd70;
		ch2_duty_low_value <= 7'd30;
	end
	else if(ch2_duty==3'b111) begin//duty 80%
		ch2_duty_high_value <= 7'd80;
		ch2_duty_low_value <= 7'd20;
	end
end

always @ (ch_choose)//choose which channel to show on 7-segment led
begin
	if(ch_choose==0) begin//show information for ch1
		/*show frequency*/
		if(ch1_fq==3'b000) begin//100 Hz
			fq3_latch <= 7'b1111001;//1
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b001) begin//200 Hz
			fq3_latch <= 7'b0100100;//2
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b010) begin//400 Hz
			fq3_latch <= 7'b0011001;//4
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b011) begin//800 Hz
			fq3_latch <= 7'b0000000;//8
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b100) begin//1600 Hz
			fq3_latch <= 7'b0000010;//6
			fq4_latch <= 7'b1111001;//1
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b101) begin//3200 Hz
			fq3_latch <= 7'b0100100;//2
			fq4_latch <= 7'b0110000;//3
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b110) begin//6400 Hz
			fq3_latch <= 7'b0011001;//4
			fq4_latch <= 7'b0000010;//6
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch1_fq==3'b111) begin//12800 Hz
			fq3_latch <= 7'b0000000;//8
			fq4_latch <= 7'b0100100;//2
			fq5_latch <= 7'b1111001;//1
		end
		/*show duty cycle*/
		if(ch1_duty==3'b000) duty2_latch <= 7'b1111001;//duty 10%
		else if(ch1_duty==3'b001) duty2_latch = 7'b0100100;//duty 20%
		else if(ch1_duty==3'b010) duty2_latch = 7'b0110000;//duty 30%
		else if(ch1_duty==3'b011) duty2_latch = 7'b0011001;//duty 40%
		else if(ch1_duty==3'b100) duty2_latch = 7'b0010010;//duty 50%
		else if(ch1_duty==3'b101) duty2_latch = 7'b0000010;//duty 60%
		else if(ch1_duty==3'b110) duty2_latch = 7'b1111000;//duty 70%
		else if(ch1_duty==3'b111) duty2_latch = 7'b0000000;//duty 80%
	end
	else begin//show information for ch2
		/*show frequency*/
		if(ch2_fq==3'b000) begin//100 Hz
			fq3_latch <= 7'b1111001;//1
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b001) begin//200 Hz
			fq3_latch <= 7'b0100100;//2
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b010) begin//400 Hz
			fq3_latch <= 7'b0011001;//4
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b011) begin//800 Hz
			fq3_latch <= 7'b0000000;//8
			fq4_latch <= 7'b1111111;//dark
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b100) begin//1600 Hz
			fq3_latch <= 7'b0000010;//6
			fq4_latch <= 7'b1111001;//1
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b101) begin//3200 Hz
			fq3_latch <= 7'b0100100;//2
			fq4_latch <= 7'b0110000;//3
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b110) begin//6400 Hz
			fq3_latch <= 7'b0011001;//4
			fq4_latch <= 7'b0000010;//6
			fq5_latch <= 7'b1111111;//dark
		end
		else if(ch2_fq==3'b111) begin//12800 Hz
			fq3_latch <= 7'b0000000;//8
			fq4_latch <= 7'b0100100;//2
			fq5_latch <= 7'b1111001;//1
		end
		/*show duty cycle*/
		if(ch2_duty==3'b000) duty2_latch = 7'b1111001;//duty 10%
		else if(ch2_duty==3'b001) duty2_latch = 7'b0100100;//duty 20%
		else if(ch2_duty==3'b010) duty2_latch = 7'b0110000;//duty 30%
		else if(ch2_duty==3'b011) duty2_latch = 7'b0011001;//duty 40%
		else if(ch2_duty==3'b100) duty2_latch = 7'b0010010;//duty 50%
		else if(ch2_duty==3'b101) duty2_latch = 7'b0000010;//duty 60%
		else if(ch2_duty==3'b110) duty2_latch = 7'b1111000;//duty 70%
		else if(ch2_duty==3'b111) duty2_latch = 7'b0000000;//duty 80%
	end
end

always @ (posedge f_clk)//set frequency by clock
begin
	/*change the wave of ch1 to high or low*/
	if(ch1_count==ch1_high_clk && f_ch1_out==1 || ch1_count==ch1_low_clk && f_ch1_out==0)
	begin
		ch1_count = 0;
		f_ch1_out = ~f_ch1_out;
	end
	else ch1_count = ch1_count + 1;
	/*change the wave of ch2 to high or low*/
	if(ch2_count==ch2_high_clk && f_ch2_out==1 || ch2_count==ch2_low_clk && f_ch2_out==0)
	begin
		ch2_count = 0;
		f_ch2_out = ~f_ch2_out;
	end
	else ch2_count = ch2_count + 1;
end

<<<<<<< HEAD
/*function romout;
	input[5:0] address;
	case(address)
	0: romout = 3;//square wave
	1: romout = 3;
	2: romout = 3;
	3: romout = 3;
	4: romout = 1;
	5: romout = 1;
	6: romout = 1;
	7: romout = 1;
	default: romout = 10'hxx;
	endcase
endfunction

always @ (posedge f_out)
	begin
		if(address==8)
			address=0;
		else
			address=address+1;
	end
*/

//-------------------------
task reset;
begin
	line = 0;
	pos = 0;
	lcden_reg = 1'b0;
	lcdrw_reg = 1'b0;
	lcdrs_reg = 1'b0;
end
endtask

task print_process;

begin
	case(state)
	reset_state:	clear_screen();
	cleared_state:	move();
	moved_state:	print();
	printed_state:	suspend();
end
endtask

task print;
input [27:0] delay_time;
begin
if(pos < 16)
	begin
		lcdrs_reg = 1'b1;
		
		lcd_reg = msg[127:120];
		msg = msg<<8;
		
		lcden_reg = 1'b1;
		
		lcden_reg = 1'b0;
		pos = pos + 1;
	end
else
	begin
		lcdrs_reg = 0;
		pos = 0;
	end
end
endtask

task set_msg;
begin
case(fq)
1:	
begin
	msg = "FREQUENCY:100HZ ";
	msg2 = "DUTY CYCLE:10%  ";
end
2:	
begin
	msg = "FREQUENCY:200HZ ";
	msg2 = "DUTY CYCLE:20%  ";
end
4:	
begin
	msg = "FREQUENCY:400HZ ";
	msg2 = "DUTY CYCLE:40%  ";
end
endcase
end
endtask

task move
begin
	lcd_reg = 8'b00000010;
	
	lcden_reg = 1'b1;
	
	lcden_reg = 1'b0;

	if(line == 1'b0)
		begin
			lcd_reg = 8'b10000000;
			
			lcden_reg = 1'b1;
	
			lcden_reg = 1'b0;
		end
	else if(line ==1'b1)
		begin
			lcd_reg = 8'b11000000;
			
			lcden_reg = 1'b1;
	
			lcden_reg = 1'b0;
		end
end
endtask

task clear_screen

begin
	lcd_reg = 8'b00000001;
	lcden_reg = 1'b1;
	lcden_reg = 1'b0;
	
	lcd_reg = 8'b00001100;
	lcden_reg = 1'b1;
	lcden_reg = 1'b0;
	
end


endtask

task suspend;
begin
	line = line + 1;
	pos = 0;	
end
endtask
//-------------------------
=======
>>>>>>> 002d9b5a5c5f6b24948d6b2f42f0df9cf449b7c5
endmodule
