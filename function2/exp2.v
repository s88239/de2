module exp2 (sw,led7_1,led7_2,led7_3);

input [11:0] sw;  //
output [6:0] led7_1,led7_2,led7_3;

reg[5:0] a ;
reg[5:0] b ;
reg[6:0] sum;

reg[6:0]led1_latch;
reg[6:0]led2_latch;
reg[6:0]led3_latch;

assign led7_1 = led1_latch; //symbol
assign led7_2 = led2_latch; //units
assign led7_3 = led3_latch; //tens

//integer sum_tens;
//integer sum_units;
reg[6:0] sumo;

initial
begin
	a = 6'd0;
	b = 6'd0;
	sum = 6'd0; 
	sumo = 0;
	//sum_tens=0;
	//sum_units=0;
	led1_latch = 7'b1111111;
	led2_latch = 7'b1000000;
	led3_latch = 7'b1111111;
end

always@(sw)
begin
if(sw[10]) //if sw2 high , do add or sub
	begin
	a = {sw[9],sw[9:5]};
	b = {sw[4],sw[4:0]};
	
	if(sw[11]==1)
	sum=a+b;
	else if(sw[11]==0)
	sum=a-b;
	
	if(sum[5]==1)  // <0
		begin
		led1_latch = 7'b0111111; //print '-'
		sumo=64-(sum[5:0]);
		//sum_tens = sumo/10;  // tens
		//sum_units = sumo%10; //units
		end
	else				// >0
		begin
	   led1_latch = 7'b1111111; //print ' '
		sumo = sum[4:0];
		//sum_tens = sumo/10;  // tens
		//sum_units = sumo%10; //units
		end
		if(sumo==32)
			begin
			led2_latch = 7'b0100100;
			led3_latch = 7'b0110000;
			end
	case(sumo)
		6'h00, 6'h0A, 6'h14, 6'h1E: led2_latch = 7'b1000000; //sumo=0,10,20,30
		6'h01, 6'h0B, 6'h15, 6'h1F: led2_latch = 7'b1111001; //sumo=1,11,21,31
		6'h02, 6'h0C, 6'h16: led2_latch = 7'b0100100;		  //sumo=2,12,22			
		6'h03, 6'h0D, 6'h17: led2_latch = 7'b0110000;		  //sumo=3,13,23
		6'h04, 6'h0E, 6'h18: led2_latch = 7'b0011001;		  //sumo=4,14,24
		6'h05, 6'h0F, 6'h19: led2_latch = 7'b0010010;		  //sumo=5,15,25
		6'h06, 6'h10, 6'h1A: led2_latch = 7'b0000010;		  //sumo=6,16,26
		6'h07, 6'h11, 6'h1B: led2_latch = 7'b1111000;		  //sum0=7,17,27	
		6'h08, 6'h12, 6'h1C: led2_latch = 7'b0000000;        //sumo=8,18,28
		6'h09, 6'h13, 6'h1D: led2_latch = 7'b0011000;		  //sumo=9,19,29
   endcase
	
	case(sumo)
		6'h1E,6'h1F: led3_latch = 7'b0110000; 					//tens='3'
		6'h14,6'h15,6'h16,6'h17,6'h18,6'h19,6'h1A,6'h1B,6'h1C,6'h1D: led3_latch = 7'b0100100; //tens='2'
		6'h0A,6'h0B,6'h0C,6'h0D,6'h0E,6'h0F,6'h10,6'h11,6'h12,6'h13: led3_latch = 7'b1111001; //tens='1'
		6'h00,6'h01,6'h02,6'h03,6'h04,6'h05,6'h06,6'h07,6'h08,6'h09: led3_latch = 7'b1000000; //tens print '0' 
	endcase
	
	end
end

endmodule