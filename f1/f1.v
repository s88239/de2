module f1 (clk_50mhz,sw,led1,led2);
input [1:0] sw;//sw[0] controls sw17, sw[1] controls sw16
input clk_50mhz;
output [6:0] led1, led2;
reg [6:0] led1_latch;
reg [6:0] led2_latch;
reg [25:0] count;
integer digit1, digit2;//digit1 record the single-digit, digit2 record the 10-digit
assign led1 = led1_latch;//later led number
assign led2 = led2_latch;//head led number

initial
begin
	digit1 = 1'd0;
	digit2 = 1'd0;
	count = 26'd0;
end

always @ (posedge clk_50mhz or posedge sw[1])//clock
begin
	if(sw[1]) begin//stop, reset counter to 00
			digit1 <= 1'd0;
			digit2 <= 1'd0;
			led1_latch <= 7'b1000000;
			led2_latch <= 7'b1000000;
			count = 26'd0;
	end
	else begin//start counting
		count <= count + 26'd1;
		if(count==26'd50000000) begin
			count <= 26'd0;
			if(sw[0]==1) begin//count upward
				if(digit1==9) begin
					digit1 <= 1'd0;
					if(digit2==9) digit2 <= 1'd0;
					else digit2 <= digit2 + 1'd1;
				end
				else digit1 <= digit1 + 1'd1;
			end
			else begin//sw[0]==0, count downward
				if(digit1==0) begin
					digit1 <= 9;
					if(digit2==0) digit2 <= 9;
					else digit2 <= digit2 - 1'd1;
				end
				else digit1 <= digit1 - 1'd1;
			end
			if(digit1==0) led1_latch <= 7'b1000000;
			else if (digit1==1) led1_latch <= 7'b1111001;
			else if (digit1==2) led1_latch <= 7'b0100100;
			else if (digit1==3) led1_latch <= 7'b0110000;
			else if (digit1==4) led1_latch <= 7'b0011001;
			else if (digit1==5) led1_latch <= 7'b0010010;
			else if (digit1==6) led1_latch <= 7'b0000010;
			else if (digit1==7) led1_latch <= 7'b1111000;
			else if (digit1==8) led1_latch <= 7'b0000000;
			else if (digit1==9) led1_latch = 7'b0011000;
			if(digit2==0) led2_latch <= 7'b1000000;
			else if (digit2==1) led2_latch <= 7'b1111001;
			else if (digit2==2) led2_latch <= 7'b0100100;
			else if (digit2==3) led2_latch <= 7'b0110000;
			else if (digit2==4) led2_latch <= 7'b0011001;
			else if (digit2==5) led2_latch <= 7'b0010010;
			else if (digit2==6) led2_latch <= 7'b0000010;
			else if (digit2==7) led2_latch <= 7'b1111000;
			else if (digit2==8) led2_latch <= 7'b0000000;
			else if (digit2==9) led2_latch <= 7'b0011000;
		end
	end
end

endmodule