module function3(clk, sw1, sw2, lcd, lcden, lcdrs, lcdrw);
	input clk;
   input [2:0] sw1;
   input [2:0] sw2;
	//input [4:0] button;
   output [7:0] lcd;
	output lcden;
   output lcdrs;
   output lcdrw;
	
	reg [7:0] lcd_words;
<<<<<<< HEAD
	reg [31:0] t;
=======
	reg t;
>>>>>>> d59b1ac5b0760b7e1cbb008724fff84d2f7f7339
	reg [7:0] pos;
	reg [2:0] pre_sw1;
	reg [2:0] pre_sw2;
	reg lcden_reg;
	reg lcdrs_reg;
	reg lcdrw_reg;
	reg [127:0] words;
	reg [1:0] state;
	reg [8:0] line;
	
	assign lcd = lcd_words;
	assign lcden = lcden_reg;
	assign lcdrs = lcdrs_reg;
	assign lcdrw = lcdrw_reg;
	
	parameter
		hello_word0 = "HELLO DSL       ",
		hello_word1 = "LDS OLLEH       ",
		idnum_word0 = "B00902098       ",
		idnum_word1 = "B00902100       ",
		idnum_word2 = "B00902050       ",
		idnum_word3 = "B00902054       ",
		idnum_word4 = "B00902048       ",
		nomem_word  = "No this member. ",
		black_word  = {16{8'b11111111}} ,
		
		reset_state = 2'b00,
		cleared_state = 2'b01,
		moved_state = 2'b10,
		displayed_state = 2'b11;
		
initial 
begin 
			pre_sw1 = 3'b000;
			pre_sw2 = 3'b000;
			
end

always @(posedge clk)
begin
	/*if(button[0] != 1'b1)
	begin
			;//msg_cnt=0;
	end
	else
	begin*/
		if(sw1 != pre_sw1 || (sw1 == 0 && sw2 != pre_sw2))
			begin
				reset();
				set_words();
				pre_sw1 = sw1;
				pre_sw2 = sw2;
		end
		else if(state == reset_state) set_words();
		
		case(sw1)
			0:	display_process(3'b100,	7'b0000000, 16, 1000000000);
			1: display_process(3'b100, 7'b0000000, 16, 1000000000);
			2: display_process(3'b100, 7'b1000000, 16, 1000000000);
			3:	
				case(state)
						reset_state: clean_screen(3'b100);
				endcase
			4:	
			begin
				case(line)
					0: display_process(3'b100, 7'b0000000, 16, 1000000);
					1:
						begin
								case(state)
									reset_state:	move(7'b1000000);
									moved_state:	print(1250000,16);
								endcase
								
						end
				endcase
			end
			5:	if(line < 16)  display_process(3'b111, line[6:0],  0,  50000000);
			6:	if(line < 16)	display_process(3'b111, {3'b100,4'b1111-line[3:0]}, 0, 50000000);
			7:	
			begin
				if(line < 256)	
				begin
					display_process(3'b100, 7'b1000000, 16, 50000000);
				end
			end
		endcase
		t = t + 1;
	//end

end


task moveleft;
begin
	case(t)
		28'd50000:	lcd_words = 8'b00001111;
		28'd60000:	lcden_reg = 1'b1;
		28'd70000:	lcden_reg = 1'b0;
	endcase
end
endtask

task set_words;
begin
	case(sw1)
	0:	
		case(sw2)
			0:	words = idnum_word0;
			1:	words = idnum_word1;
			2:	words = idnum_word2;
			3:	words = idnum_word3;
			4:	words = idnum_word4;
			default: words = nomem_word;
		endcase
	1:	words = hello_word0;
	2:	words = hello_word1;
	3:	/*nothing*/;
	4:	words = black_word;
	5:	/*move the cursor from left to right*/;
	6:	/*move the cursor from right to left*/;
	7:	words = {"      0x",hexdit(line/16),hexdit(line%16),"      "};
	endcase
end
endtask

function [7:0] hexdit;
input [3:0] number;

begin
case(number)
0: hexdit = "0";	
1:	hexdit = "1";
2:	hexdit = "2";
3:	hexdit = "3";
4:	hexdit = "4";
5:	hexdit = "5";
6:	hexdit = "6";
7:	hexdit = "7";
8:	hexdit = "8";
9:	hexdit = "9";
10: hexdit = "A";
11: hexdit = "B";
12: hexdit = "C";
13: hexdit = "D";
14: hexdit = "E";
15: hexdit = "F";
endcase
end
endfunction


task display_process;
input [2:0] cursor;
input [6:0] address;
input [7:0]	length;
input [31:0] sleep;

begin
	case(state)
		reset_state: clean_screen(cursor);
		cleared_state:	move(address);
		moved_state:	print(1250000,length);
		displayed_state:	take_rest(sleep);
	endcase
end
endtask

task move;
input [6:0]address;
begin
	case(t)
	28'd50000:	lcd_words = 8'b00000010;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000:	lcd_words = {1'b1, address};
	28'd180000: lcden_reg = 1'b1;
	28'd190000: lcden_reg = 1'b0;
	28'd200000:	
		begin
			state = moved_state;
			t = 0;
		end
	endcase
end
endtask
task reset;
begin
	state = reset_state;
	lcden_reg = 1'b0;
	lcdrs_reg = 1'b0;
	lcdrw_reg = 1'b0;
	pos = 7'b0000000;
	t  = 0;
	line = 0;
end
endtask

task clean_screen;
input [2:0] cursor;

begin
case(t)
	28'd50000:	lcd_words = 8'b00000001;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000: lcd_words = {5'b00001,cursor};
	28'd180000:	lcden_reg = 1'b1;
	28'd190000:	lcden_reg = 1'b0;
	28'd200000: 
		begin
			state = cleared_state;
			t = 0;
		end
endcase
end
endtask

task print;
input [27:0] delay;
input [7:0] length;
	
begin
	if(t == delay)
	begin
		t = 0;
	end
	else 
	begin
		if(pos < length)
		begin
			case(t)
				28'd50000: lcdrs_reg = 1'b1;
				28'd60000: 
					begin
					lcd_words = words[127:120];
					words = words << 8;
					end
				28'd70000: lcden_reg = 1'b1;
				28'd80000:
				begin
					lcden_reg = 1'b0;
					pos = pos + 1;
				end
			endcase
		end
		
		else
		begin
			state = displayed_state;
			lcdrs_reg = 1'b0;
			//pos = 0;
			t = 0;
		end
	end
end
endtask

task take_rest;
input [31:0] sleep;
begin
	if(t == sleep)
	begin
		state = reset_state;
		pos = 0;
		t = 0;
		line = line + 1;
	end
end
endtask
endmodule
