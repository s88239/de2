module function5(clk, channel, sw1, sw2, sw3, sw4, lcd, lcden, lcdrs, lcdrw, button);
	input clk;
	input channel;
   input [2:0] sw1;
   input [2:0] sw2;
	input [2:0] sw3;
	input [2:0] sw4;
	input [4:0] button;
   output [7:0] lcd;
	output lcden;
   output lcdrs;
   output lcdrw;
	
	reg [7:0] lcd_words;
	reg t;
	reg [7:0] pos;
	reg [2:0] pre_sw1;
	reg [2:0] pre_sw2;
	reg [2:0] pre_sw3;
	reg [2:0] pre_sw4;
	reg lcden_reg;
	reg lcdrs_reg;
	reg lcdrw_reg;
	reg [127:0] words;
	reg [127:0] words2;
	reg [1:0] state;
	reg [8:0] line;
	
	assign lcd = lcd_words;
	assign lcden = lcden_reg;
	assign lcdrs = lcdrs_reg;
	assign lcdrw = lcdrw_reg;
	
	parameter
	
		f_word0 = "FREQUENCY:100Hz ",
		f_word1 = "FREQUENCY:200Hz ",
		f_word2 = "FREQUENCY:400Hz ",
		f_word3 = "FREQUENCY:800Hz ",
		f_word4 = "FREQUENCY:1600Hz",
		f_word5 = "FREQUENCY:3200Hz",
		f_word6 = "FREQUENCY:6400Hz",
		f_word7 = "FREQUENCY12800Hz",
		
		d_word0 = "DUTY CYCLE:10%  ",
		d_word1 = "DUTY CYCLE:20%  ",
		d_word2 = "DUTY CYCLE:30%  ",
		d_word3 = "DUTY CYCLE:40%  ",
		d_word4 = "DUTY CYCLE:50%  ",
		d_word5 = "DUTY CYCLE:60%  ",
		d_word6 = "DUTY CYCLE:70%  ",
		d_word7 = "DUTY CYCLE:80%  ",
	
		reset_state = 2'b00,
		cleared_state = 2'b01,
		moved_state = 2'b10,
		displayed_state = 2'b11;
		
initial 
begin 
			pre_sw1 = 3'b000;
			pre_sw2 = 3'b000;
			
end

always @(posedge clk)
begin
	if(button[0] != 1'b1)
	begin
			;//msg_cnt=0;
	end
	else
	begin
		if((sw1 != pre_sw1) || (sw2 != pre_sw2) || (sw3 != pre_sw3) || (sw4 != pre_sw4) )
			begin
				reset();
				set_words();
				pre_sw1 = sw1;
				pre_sw2 = sw2;
				pre_sw3 = sw3;
				pre_sw4 = sw4;
		end
		else if(state == reset_state) set_words();
		
		case(line)
		0:	display_process(3'b100, 7'b0000000, 16, 10000);//1000000000
		1:
			begin
								case(state)
									reset_state:	move(7'b1000000);
									moved_state:	print(1250000,16);
								endcase
								
						end
		endcase
		t = t + 1;
	end

end

task moveleft;
begin
	case(t)
		28'd50000:	lcd_words = 8'b00001111;
		28'd60000:	lcden_reg = 1'b1;
		28'd70000:	lcden_reg = 1'b0;
	endcase
end
endtask

task set_words;
begin
if(channel == 1'b0)
begin
	case(sw1)
	0:words = f_word0;
	1:words = f_word1;
	2:words = f_word2;
	3:words = f_word3;
	4:words = f_word4;
	5:words = f_word5;
	6:words = f_word6;
	7:words = f_word7;
	endcase
	
	case(sw2)
	0:words2 = d_word0;
	1:words2 = d_word1;
	2:words2 = d_word2;
	3:words2 = d_word3;
	4:words2 = d_word4;
	5:words2 = d_word5;
	6:words2 = d_word6;
	7:words2 = d_word7;
	endcase
end
else if(channel == 1'b1)
begin
	
	case(sw3)
	0:words = f_word0;
	1:words = f_word1;
	2:words = f_word2;
	3:words = f_word3;
	4:words = f_word4;
	5:words = f_word5;
	6:words = f_word6;
	7:words = f_word7;
	endcase
	
	case(sw4)
	0:words2 = d_word0;
	1:words2 = d_word1;
	2:words2 = d_word2;
	3:words2 = d_word3;
	4:words2 = d_word4;
	5:words2 = d_word5;
	6:words2 = d_word6;
	7:words2 = d_word7;
	endcase
end
end
endtask

task display_process;
input [2:0] cursor;
input [6:0] address;
input [7:0]	length;
input [31:0] sleep;

begin
	case(state)
		reset_state: clean_screen(cursor);
		cleared_state:	move(address);
		moved_state:	print(1250000,length);
		displayed_state:	take_rest(sleep);
	endcase
end
endtask

task move;
input [6:0]address;
begin
	case(t)
	28'd50000:	lcd_words = 8'b00000010;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000:	lcd_words = {1'b1, address};
	28'd180000: lcden_reg = 1'b1;
	28'd190000: lcden_reg = 1'b0;
	28'd200000:	
		begin
			state = moved_state;
			t = 0;
		end
	endcase
end
endtask
task reset;
begin
	state = reset_state;
	lcden_reg = 1'b0;
	lcdrs_reg = 1'b0;
	lcdrw_reg = 1'b0;
	pos = 7'b0000000;
	t  = 0;
	line = 0;
end
endtask

task clean_screen;
input [2:0] cursor;

begin
case(t)
	28'd50000:	lcd_words = 8'b00000001;
	28'd60000:	lcden_reg = 1'b1;
	28'd70000:	lcden_reg = 1'b0;
	
	28'd170000: lcd_words = {5'b00001,cursor};
	28'd180000:	lcden_reg = 1'b1;
	28'd190000:	lcden_reg = 1'b0;
	28'd200000: 
		begin
			state = cleared_state;
			t = 0;
		end
endcase
end
endtask

task print;
input [27:0] delay;
input [7:0] length;
	
begin
	if(t == delay)
	begin
		t = 0;
	end
	else 
	begin
		if(pos < length)
		begin
			case(t)
				28'd50000: lcdrs_reg = 1'b1;
				28'd60000: 
					begin
						case(line)
						0:
						begin
							lcd_words = words[127:120];
							words = words << 8;
						end
						1:
						begin
							lcd_words = words2[127:120];
							words2 = words2 << 8;
						end
						endcase
					end
				28'd70000: lcden_reg = 1'b1;
				28'd80000:
				begin
					lcden_reg = 1'b0;
					pos = pos + 1;
				end
			endcase
		end
		
		else
		begin
			state = displayed_state;
			lcdrs_reg = 1'b0;
			//pos = 0;
			t = 0;
		end
	end
end
endtask

task take_rest;
input [31:0] sleep;
begin
	if(t == sleep)
	begin
		state = reset_state;
		pos = 0;
		t = 0;
		line = line + 1;
	end
end
endtask
endmodule
